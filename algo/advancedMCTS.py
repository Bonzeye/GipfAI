
# implementation of the final player based on MCTS

from copy import copy,deepcopy

from engine.game import DvonnGame
from engine.players import RandomPlayer

from tools.gametree import TreeNode,GameTree
from tools.zobristhash import Hash

class Bot_AdvancedMCTS:

    """
    this bot is the final release of the MCTS-based algorithm for playing Dvonn
    the algorithm is described as follow :
    given a game :
    - if it is the first time playing this instance, the player generate the base gametre

    """
    def __init__(self,nb_sim=100,player=0,verbose=False,threshold=100):
        self.name = "Bot_AdvancedMCTS"
        self.nb_sim = nb_sim
        self.gametree = GameTree(player=player)
        self.hashing = Hash()
        self.tt = dict()
        self.last_instance = None
        self.verbose = verbose
        self.threshold = threshold

    def play(self,game):

        # initially, check if gametree is matching game is correctly instancied
        if self.gametree.root.comp != game.player:
            raise ValueError("Wrong player turn")

        if self.last_instance is not None:
            # put the gametree to the correct place
            if self.verbose:
                print("> Selecting proper cell")

            selected = False
            possibilities = self.last_instance.possible_play()
            if possibilities == list():
                possibilities = ["pass"]

            for start,end in possibilities:

                choice = "{} {} {} {} {}".format(self.last_instance.player,start[0],start[1],end[0],end[1])
                copy_game = deepcopy(self.last_instance)
                copy_game.play(choice)

                game_hash = self.hashing.hash(game)

                if self.hashing.hash(copy_game) == game_hash:

                    selected = True

                    if not choice in self.gametree.current.sons.keys():

                        self.gametree.add_son(choice)

                    self.gametree.go_son(choice)

                    if self.verbose:
                        print("> Selection complete")
                        print("  current node {} is a {} node".format(choice,["MAX","MIN"][self.gametree.current.comp]))
                    break

            if not selected:

                raise ValueError("Fucking Error !!!")

        # now cut all prior branchs
        self.gametree.root = self.gametree.current # garbage coll should handle

        # check if simulations are needed from this node
        if self.gametree.current.complete:

            # if this node is already fully computed
            if self.verbose:
                print("> Current position is complete with value {}".format(self.gametree.current.value))

            choice = self.gametree.best_son()

        else:
            # here is the big work
            if self.verbose:
                print("> Enterig the wormhole ...")

            for i in range(self.nb_sim):

                if self.verbose:
                    print("\n###### STARTING SIMULATION {} ######\n".format(i))

                current_backup = copy(self.gametree.current)
                simu_game = deepcopy(game)
                simu_player = RandomPlayer()
                if self.verbose:
                    i = int()

                while not self.gametree.current.complete:

                    if self.verbose:
                        i += 1

                    simu_choice = simu_player.play(simu_game)
                    simu_game.play(simu_choice)

                    # check if new position is in transposition table
                    simu_hash = self.hashing.hash(simu_game)
                    if simu_hash in self.tt.keys():

                        if self.verbose:
                            print("[{}] +++ FOUND in transposition table +++".format(i))

                        self.gametree.current.sons[simu_choice] = self.tt[simu_hash]

                    else:

                        if self.verbose:
                            print("[{}] --- NOT in transposition table ---".format(i))

                        self.gametree.add_son(simu_choice)
                        self.tt[simu_hash] = self.gametree.current.sons[simu_choice]

                    self.gametree.go_son(simu_choice)

                    # check if it is a endgame we can fully resolve
                    if not self.gametree.current.complete and len(simu_game.possible_play(pl=0)) * len(simu_game.possible_play(pl=1)) <= self.threshold:

                        if self.verbose:
                            print("[{}] ### NOW BUILDING COMPLETE PART OF TREE ###\n".format(i))

                        self.gametree.build_from_game(simu_game,hashing=self.hashing,tt=self.tt)

                        if self.verbose:
                            print("[{}] ### MINIMAX COMPUTING ###".format(i))

                        self.gametree.minimax()



                # restore the good position
                self.gametree.current = copy(current_backup)
                self.gametree.minimax()

            choice = self.gametree.best_son()

            if self.verbose:
                print("-------------------------------------------------------")
                print(" current node is a {} node".format(["MAX","MIN"][self.gametree.current.comp]))
                print("Values of sons : ")
                for key,value in self.gametree.current.sons.items():
                    print(" * {} = {}".format(key,value.value))
                print("> Best son is {} with value {}".format(choice,self.gametree.current.sons[choice].value))
                print("-------------------------------------------------------")


        # now, update all attributes
        copy_game = deepcopy(game)
        copy_game.play(choice)
        self.last_instance = deepcopy(copy_game)
        self.gametree.go_son(choice)

        return choice
