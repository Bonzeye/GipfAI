from engine.game import *
from engine.players import *

from tools.gametree import *

class MCTS_player:

    def __init__(self,nb_sim=100):

        self.name = "MCTS_player"
        self.nb_sim = nb_sim

    def play(self,game):

        tree = GameTree()

        for i in range(self.nb_sim):

            copy_game = deepcopy(game)
            player = RandomPlayer()

            while not copy_game.is_finished():

                choice = player.play(copy_game)
                copy_game.play(choice)

                if choice in tree.current.sons.keys():

                    tree.go_son(choice)

                else:

                    tree.add_son(choice)
                    tree.go_son(choice)

                    # affecte node for minimax purpose (as max or min player)
                    tree.current.comp = (copy_game.player + 1) % 2

            tree.current.value = copy_game.score()[0] - copy_game.score()[1]
            tree.go_root()

        results = dict()

        for choice,son in tree.current.sons.items():

            results[choice] = son.alpha_beta()

        if game.player is 0:

            best_value = max(results.values())

        else:

            best_value = min(results.values())

        for choice,value in results.items():

            if value == best_value:

                return choice

class MCTS_player_v2:

    def __init__(self,nb_sim=100):

        self.name = "MCTS_player_v2"
        self.nb_sim = nb_sim

    def play(self,game):

        tree = GameTree()

        # debug print
        #print("Starting simulations")

        if len(game.possible_play(pl=0)) + len(game.possible_play(pl=1)) <= 100:

            # make full resolution
            pass

        for i in range(self.nb_sim):

            copy_game = deepcopy(game)
            player = RandomPlayer()

            while not copy_game.is_finished():

                choice = player.play(copy_game)
                copy_game.play(choice)

                if choice in tree.current.sons.keys():

                    # exit while loop if this simulation is going to a solved son
                    if tree.current.sons[choice].value is not None:
                        break

                    tree.go_son(choice)

                else:

                    tree.add_son(choice)
                    tree.go_son(choice)

                    # affect node for minimax purpose (as max or min player)
                    tree.current.comp = (copy_game.player + 1) % 2

                    # if it's an end game, compute full minimax solving
                    if len(copy_game.possible_play(pl=0)) + len(copy_game.possible_play(pl=1)) <= 100:

                        while tree.current.value is None:


                            pass
                            #tree.current.value = copy_game.score()[0] - copy_game.score()[1]
            tree.go_root()

        results = dict()

        for choice,son in tree.current.sons.items():

            results[choice] = son.alpha_beta()

        if game.player is 0:

            best_value = max(results.values())

        else:

            best_value = min(results.values())

        # debug print
        #print("results :")
        #for choice, value in results.items():
        #    print("{} : {}".format(choice, value))



        for choice,value in results.items():

            if value == best_value:

                return choice
