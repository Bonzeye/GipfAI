from algo.mctreesearch import *
from algo.pureplayer import *

from engine.game import *
from engine.players import *

class MixPlayer:

    def __init__(self,nb_sim=100,verbose=False):
        self.name = "Mix_player"
        self.player = MCTS_player(nb_sim=100)
        self.state = "MC" # between "FULL" and "MC"
        self.verbose = verbose

    def play(self,game):

        size = len(game.possible_play(pl=0)) * len(game.possible_play(pl=1))
        if self.verbose:
            print("size = {}".format(size))

        if self.state is "MC" and (size <= 100):
            self.state = "FULL"
            self.player = pureplayer(verbose=self.verbose)

        return self.player.play(game)
