from copy import copy,deepcopy
from random import randrange, random
import operator

from dvonn_game import *
from dvonn_players import *



# A basic implementation of a Monte Carlo based algorithm to compute play
# of an artificial player

class MC_player:
    def __init__(self,nb_sim=0,verbose=False):
        """
        if nb_sim == 0, the nb of simulations is responsive
        """
        if nb_sim == 0:
            self.adaptative = True
            self.round = 0
        else:
            self.adaptative = False


        self.verbose = verbose
        self.name = "MC_player"
        self.nb_sim = nb_sim

    def play(self,game):
        """
        the choice is based on n simulations
        """
        if self.verbose:
            print("{} turn".format(self.name))

        if self.adaptative:
            self.round += 1
            self.nb_sim = int(10 ** (1 + 3 * self.round/23))

        current_player = game.player
        results = dict()

        # step 1 : generate all possibilities
        opts = game.possible_play()
        if opts == list():
            return 'pass'

        # step 2 : for each possibility, generate n simulations

        for simulation in opts:

            simu_game = deepcopy(game)

            #print("*starting simulations for {}".format(simulation))
            #print(simu_game)

            simu_game.play("{} {} {} {} {}".format(current_player,
                                                    simulation[0][0],
                                                    simulation[0][1],
                                                    simulation[1][0],
                                                    simulation[1][1]))

            results[simulation] = 0
            #print("-----")
            #print(simu_game)
            #print("-----")

            for n in range(self.nb_sim):
                tmp_game = deepcopy(simu_game)
                res = tmp_game.run(RandomPlayer(),RandomPlayer())

                if res[0] > res[1] and current_player is 0:
                    results[simulation] += 1
                if res[1] > res[0] and current_player is 1:
                    results[simulation] += 1

        if self.verbose:
            print("simulation results")
            for key,value in results.items():
                print("{}:{}".format(key,value))

        # step3 : return the best result in the simulations
        maximum = max(results.values())
        for opt,succ in results.items():
            if succ == maximum:
                return "{} {} {} {} {}".format(current_player,
                                                        opt[0][0],
                                                        opt[0][1],
                                                        opt[1][0],
                                                        opt[1][1])

    def place(self,game):
       pass

class Stoch1_MC_player:
    def __init__(self,nb_sim=0,verbose=False):
        """
        if nb_sim == 0, the nb of simulations is responsive
        """
        if nb_sim == 0:
            self.adaptative = True
            self.round = 0
        else:
            self.adaptative = False


        self.name = "Stoch1_MC_player"
        self.nb_sim = nb_sim
        self.verbose = verbose

    def play(self,game):
        """
        the choice is based on n simulations
        """
        if self.verbose:
            print("{} turn".format(self.name))

        if self.adaptative:
            self.round += 1
            self.nb_sim = int(10 ** (1 + 3 * self.round/23))

        current_player = game.player
        results = dict()
        prob_tab = dict()

        # step 1 : generate all possibilities
        opts = game.possible_play()
        if opts == list():
            return 'pass'

        if len(opts) is 1:
            return "{} {} {} {} {}".format(current_player,opts[0][0][0],opts[0][0][1],opts[0][1][0],opts[0][1][1])

        # step 2 : for each possibility, generate n simulations

        for simulation in opts:

            simu_game = deepcopy(game)

            #print("*starting simulations for {}".format(simulation))
            #print(simu_game)

            simu_game.play("{} {} {} {} {}".format(current_player,
                                                    simulation[0][0],
                                                    simulation[0][1],
                                                    simulation[1][0],
                                                    simulation[1][1]))

            results[simulation] = 0
            #print("-----")
            #print(simu_game)
            #print("-----")

            for n in range(self.nb_sim):
                tmp_game = deepcopy(simu_game)
                res = tmp_game.run(RandomPlayer(),RandomPlayer())

                if res[0] > res[1] and current_player is 0:
                    results[simulation] += 1
                if res[1] > res[0] and current_player is 1:
                    results[simulation] += 1


        # step3 : return the best result in the simulations
        bound = 0
        for opt,succ in results.items():
            prob_tab[bound+succ]="{} {} {} {} {}".format(current_player,
                                                    opt[0][0],
                                                    opt[0][1],
                                                    opt[1][0],
                                                    opt[1][1])
            bound += succ

        if self.verbose:
            print("simulation results")

            for number in sorted(list(prob_tab.keys())):
                print("{}:{}".format(number,prob_tab[number]))

        if max(prob_tab.keys()) is 0:
            return prob_tab[0]

        roll = randrange(max(prob_tab.keys()))

        if self.verbose:
            print(roll)

        for prob,choice in prob_tab.items():
            if roll <= prob:
                return choice

    def place(self,game):
       pass

class Stoch2_MC_player:
    def __init__(self,nb_sim=0,verbose=False):
        """
        if nb_sim == 0, the nb of simulations is responsive
        """
        if nb_sim == 0:
            self.adaptative = True
            self.round = 0
        else:
            self.adaptative = False


        self.name = "Stoch2_MC_player"
        self.nb_sim = nb_sim
        self.verbose = verbose

    def play(self,game):
        """
        the choice is based on n simulations
        """

        if self.verbose:
            print("{} turn".format(self.name))

        if self.adaptative:
            self.round += 1
            self.nb_sim = int(10 ** (1 + 3 * self.round/23))

        current_player = game.player
        results = dict()
        prob_tab = dict()

        # step 1 : generate all possibilities
        opts = game.possible_play()
        if opts == list():
            return 'pass'

        if len(opts) is 1:
            return "{} {} {} {} {}".format(current_player,opts[0][0][0],opts[0][0][1],opts[0][1][0],opts[0][1][1])

        # step 2 : for each possibility, generate n simulations

        for simulation in opts:

            simu_game = deepcopy(game)


            simu_game.play("{} {} {} {} {}".format(current_player,
                                                    simulation[0][0],
                                                    simulation[0][1],
                                                    simulation[1][0],
                                                    simulation[1][1]))

            results[simulation] = 0

            for n in range(self.nb_sim):
                tmp_game = deepcopy(simu_game)
                res = tmp_game.run(RandomPlayer(),RandomPlayer())

                if res[0] > res[1] and current_player is 0:
                    results[simulation] += 1
                if res[1] > res[0] and current_player is 1:
                    results[simulation] += 1

        if self.verbose:
            print("simulation results")
            for key,value in results.items():
                print("{}:{}".format(key,value))

        # step3 : return the best result in the simulations
        prob_tab = prob_calc(results, self.nb_sim)

        bound = 0
        cumul_prob_tab = dict()

        for choice, prob in prob_tab.items():

            cumul_prob_tab[choice] = prob + bound
            bound += prob



        if self.verbose:

            print("probability table")

            sorted_cumul_prob_tab = sorted(cumul_prob_tab.items(), key=operator.itemgetter(1))

            for choice,value in sorted_cumul_prob_tab:
                print("{}:{:.2%} ({:.2%})".format(choice,value,prob_tab[choice]))


        if max(prob_tab.values()) is 0:
            return convert_choice(current_player,list(prob_tab.keys())[0])

        roll = random()

        if self.verbose:
            print(roll)

        for choice,prob in cumul_prob_tab.items():
            if roll <= prob:
                return convert_choice(current_player,choice)

        print("if you are here, there is obviously a huge error")


    def place(self,game):
       pass

def prob_calc(results,nb_sim):
    """
    compute the probability that probability of win with result is
    better than the probability of win of every other result, based on
    the confidency interval
    """

    # first step, compute the confidency intervall for every result

    conf_int = dict()

    for choice, value in results.items():
        conf_int[choice] = (value/nb_sim - (nb_sim) ** (-1/2),
                            value/nb_sim + (nb_sim) ** (-1/2))

    # second step, compute the probability of better probability of win for
    # a choice

    prob_tab = dict()

    for choice, interval in conf_int.items():

        prob = 1.

        for sub_choice, sub_interval in conf_int.items():

            if sub_choice != choice:

                a_j = interval[0]
                b_j = interval[1]

                a_i = sub_interval[0]
                b_i = sub_interval[1]

                a_k = max(a_j,a_i)
                b_k = min(b_j,b_i)

                #prob *= ((b_j - a_j)*(a_k - a_i) + 1/2 * (b_k - a_k) ** 2 + (b_i - a_i)*(b_j - b_k)) / ((b_i - a_i)*(b_j - a_j))
                prob *= (b_j - b_k)/(b_j - a_j) + (b_k - a_k)/(b_j - a_j) * ((a_k - a_i)/(b_i - a_i) + 1/2 * (b_k - a_k)/(b_i - a_i))
        prob_tab[choice] = copy(prob)

    return prob_tab

def convert_choice(player,start_end):

    """
    transform a (start,end) field to a string field for player
    """

    return "{} {} {} {} {}".format(player,start_end[0][0],start_end[0][1],start_end[1][0],start_end[1][1])
