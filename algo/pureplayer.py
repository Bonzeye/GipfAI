from copy import deepcopy

from tools.gametree import GameTree



# a pure minimax player trying to exhaust all possibilities

class pureplayer:

    def __init__(self,verbose=False,player=0):

        self.name = "pure_player"
        self.gametree = None
        self.last_instance = None
        self.verbose = verbose
        self.player = player

    def play(self,game):

        self.player = game.player

        # chck if the tree has already been build. Otherwise build it
        if self.gametree is None:

            init = True

            if self.verbose:
                print(">> Building full gametree\n")

            self.gametree = GameTree(player=self.player)
            self.last_instance = deepcopy(game)
            self.gametree.build_from_game(game,verbose=self.verbose)

            if self.verbose:
                print("\n>> Build complete")



        else:
            # put gametree.current to the proper cell

            init = False

            if self.verbose:
                print(">> Selecting proper cell")

            i = 0
            for choice in self.gametree.current.sons.keys():

                copy_game = deepcopy(self.last_instance)
                copy_game.play(choice)

                if copy_game == game:

                    self.gametree.go_son(choice)
                    break

                i += 1
                if i == len(self.gametree.current.sons.keys()):
                    print("### FUCKING ERROR ###")

            if self.verbose:
                print(">> Selection done")

            # now cut all prior branchs
            self.gametree.root = self.gametree.current # garbage coll should handle

        # now compute the minimax for the current position

        if self.verbose:
            print(">> Start computing best choice")

        choice = self.gametree.best_son()

        if self.verbose:
            print(">> Computing complete found choice {}".format(choice))

        if init:
            copy_game = deepcopy(self.last_instance)

        copy_game.play(choice)
        self.last_instance = deepcopy(copy_game)

        self.gametree.go_son(choice)

        return choice
