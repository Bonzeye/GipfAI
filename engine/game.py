
from copy import copy, deepcopy
from random import shuffle

vectors = [(1,0),(0,1),(1,1),(-1,0),(0,-1),(-1,-1)]
exclude = [(4,0),(4,1),(3,0),(0,9),(0,10),(1,10)]


class DvonnGame:

    def __init__(self):

        """
        player is the current player to play, 0 is white and 1 is black
        positions are coded as (row,col) keys in board dict
        content of positions are coded as (color,size,red)
            color = 0 for white, 1 for black, 2 for red
            size is the number of pieces in the pile
            red is True if a red piece is in the pile
        opt is the registering of availalble possible move of every player
        a 2-dict containg items of (row,col):list((row,col)) (start:list(end))
        state can be 'place', 'play' or 'end'
        """

        self.player = 0
        self.board = dict()
        self.opt = [list(),list()]
        self.state = 'place'
        self.passing = 0
        self.round = 0

        self.log = None
        self.setup = None


        for row in range(5):
            for col in range(11):

                if not (row,col) in exclude:

                    self.board[(row,col)] = None

    def __str__(self):
        text = list()
        sort = sorted(list(self.board.keys()),key=lambda x:x[0]+x[1]/100)
        for coord in sort:
            if self.board[coord] is None:
                text.append("{} {} : None".format(coord[0],coord[1]))
            else:
                text.append("{} {} : {} {} {}".format(coord[0],coord[1],self.board[coord][0],self.board[coord][1],self.board[coord][2]))
        return "\n".join(text)

    def __eq__(self,game):

        if self.setup is None or self.log is None:

            for coord,cell in self.board.items():

                if not coord in game.board.keys() or game.board[coord] != cell:
                    return False

        if self.setup == game.setup and self.log == game.log:

            return True

    # OPERATING TOOLS FOR DVONN GAME MANAGEMENT

    def generate_opt(self):
        """
        generate opt lists
        """
        for coord,cell in self.board.items():

            if len(self.get_neigh(coord)) < 6 and cell[0] is not 2:
                self.opt[cell[0]].append(copy(coord))

    def random_setup(self):

        """place starting pieces at random"""

        border = [(0,0),(0,1),(0,2),(0,3),(0,4),(0,5),(0,6),(0,7),(0,8),
                  (1,0),(1,9),
                  (2,0),(2,10),
                  (3,1),(3,10),
                  (4,2),(4,3),(4,4),(4,5),(4,6),(4,7),(4,8),(4,9),(4,10)]
        pieces = list()

        for i in range(3):
            pieces.append((2,1,True))
        for i in range(23):
            pieces.append((1,1,False))
            pieces.append((0,1,False))
        shuffle(pieces)

        for row in range(5):
            for col in range(11):

                if not (row,col) in exclude:

                    self.board[(row,col)] = pieces.pop()

        # initilization of self.opt with bordering pieces of player color
        for coord in border:
            if self.board[coord][0] is not 2:
                self.opt[self.board[coord][0]].append(coord)

        if self.log is not None:
            self.setup = deepcopy(self.board)

        self.state = 'play'

    def available_moves(self,coord):

        """
        return all possible coordonates of a legal move for the piece
        """

        res = list()
        row,col = coord
        u = self.board[coord][1]

        for v in vectors:

            w = (row + v[0]*u , col+v[1]*u)

            if w in self.board.keys():

                res.append(copy(w))

        return res

    def get_neigh(self,coord):
        """
        get all neighbors around (coord)
        """
        res = list()

        for v in vectors:
            neigh_coord = (coord[0]+v[0],coord[1]+v[1])
            if neigh_coord in self.board.keys():
                res.append(copy(neigh_coord))

        return res

    def remove(self,coord):
        """
        remove the piece at coord position and update the border
        """

        if self.board[coord][0] is 2:
            print("***ERROR***")
            print("{} is a Dvonn piece".format(coord))

        if coord in self.opt[1]:
            self.opt[1].remove(coord)
        elif coord in self.opt[0]:
            self.opt[0].remove(coord)

        del self.board[coord]


        for neigh in self.get_neigh(coord):

            if neigh in self.board.keys() and self.board[neigh][0] is not 2:
                if neigh not in self.opt[self.board[neigh][0]]:
                    self.opt[self.board[neigh][0]].append(neigh)

    def possible_play(self,pl=None):

        """
        for every item of opt for the player, check all move possibilities
        """

        if pl is None:
            player = self.player
        else:
            player = pl

        res = list()

        for start in self.opt[player]:
            for end in self.available_moves(start):
                res.append((start,end))

        return res

    def get_red(self):
        """
        return a 3-list of dvonn piece coordonates
        """
        res = list()

        for key,value in self.board.items():
            if value[2]:
                res.append(copy(key))
        return res

    def is_finished(self):
        """
        True if the game is finished, False elsewhere
        """
        if self.passing == 2:
            return True
        elif len(self.opt[0]) + len(self.opt[1]) == 0:
            return True
        return False

    def score(self):
        """
        return (white_score,black_score)
        """
        score = [int(),int(),int()]

        for pile in self.board.values():
            score[pile[0]] += pile[1]

        return score[0],score[1]

    # STANDARD GAME PROCEDURES

    def play(self,choice):

        """
        choice is a string as "player start_row start_col end_row end_col"
        or is "pass" if the player choice to pass
        """

        if choice is 'pass':
            self.player = (self.player + 1) % 2
            self.passing += 1
            self.round += 1
            return "Pass."

        player, start_row, start_col, end_row, end_col = [int(item) for item in choice.split()]
        start = (start_row, start_col)
        end = (end_row, end_col)

        if player != self.player:
            return "Wrong player turn"

        if not start in self.opt[self.player]:
            return "Starting piece is not playable."

        if not end in self.available_moves(start):
            return "Ending place is not valid."

        #if self.board[start][2] and self.board[end][2]:
        #    print("{} and {} Dvonn piece are placed together")

        if end in self.opt[(self.player + 1) % 2]:
            self.opt[(self.player + 1) % 2].remove(end)
            self.opt[self.player].append(end)



        self.board[end] = (self.board[start][0],
                           self.board[start][1] + self.board[end][1],
                           self.board[start][2] or self.board[end][2])

        self.remove(start)
        if end in self.opt[(self.player + 1) % 2]:
            self.opt[(self.player + 1) % 2].remove(end)

        self.connexity_update(start)
        self.player = (self.player + 1) % 2
        self.passing = 0
        self.round += 1

        to_remove = list()
        for item in self.opt[0]:
            if item not in self.board.keys():
                to_remove.append(copy(item))
        for item in to_remove:
            self.opt[0].remove(item)

        to_remove = list()
        for item in self.opt[1]:
            if item not in self.board.keys():
                to_remove.append(copy(item))
        for item in to_remove:
            self.opt[1].remove(item)

        if self.log is not None:
            self.log.append(choice)

        return "Move done"

    def run(self,w_player,b_player,log=True,verbose=False):
        """
        will run a complete game with w_player and b_player
        currently on a random placement mode
        """
        players = [w_player,b_player]

        if log:
            self.log = list()

        if self.state is 'place':

            self.random_setup()


        while not self.is_finished():

            choice = players[self.player].play(self)

            if verbose:
                print("***ROUND {}***".format(self.round))
                print("player {}".format(self.player))
                print("state of game :")
                print(self)
                print("number of possibilities : {}".format(len(self.possible_play())))
                print("choice of player {}".format(self.player))
                print(choice)

            result = self.play(choice)
            if verbose:
                print(result)
                if "Move done" not in result and "Pass" not in result:
                    input()

        return self.score()

    # CONNEXITY VERIFICATION TOOLS

    def are_connected(self,start,end):
        """
        a djikstra based path algorithm betwen start and end wich return True if
        a path exists from start to end, and False elsewhere
        this function also return the list of parcoured cells
        """
        explored = list()
        to_explore = [start]
        uncovered = list(self.board.keys())
        if start in uncovered:
            uncovered.remove(start)

        # debug print
        #print("Starting connection algorithm between {} and {}".format(start,end))

        while len(to_explore) is not 0 and end in uncovered:

            current = to_explore.pop()

            # debug print
            #print("current = {}".format(current))
            #print("to_explore = {}".format(to_explore))
            #print("explored = {}".format(explored))
            #print("uncovered = {}".format(uncovered))
            #print("-----")
            #input()

            for neigh in self.get_neigh(current):

                if neigh in uncovered:

                    uncovered.remove(neigh)
                    to_explore.append(copy(neigh))

            explored.append(copy(current))

        if end in uncovered:
            return (False, explored + to_explore)
        else:
            return (True, explored + to_explore)

    def neigh_connex(self,coord):
        """
        return the number of connex parts of the direct neighbors
        """
        neigh_list = self.get_neigh(coord)

        neigh_place = [int()]*6

        for v in vectors:

            if (coord[0]+v[0],coord[1]+v[1]) in neigh_list:

                neigh_place[vectors.index(v)] = 1



        conn_nb = 1
        for index in range(6):
            if neigh_place[index] is 1:
                neigh_place[index] = conn_nb
            elif neigh_place[index] is 0:
                conn_nb += 1

        if neigh_place[0] is not 0 and neigh_place[-1] is not 0:
            for index in range(1,6):
                if neigh_place[index] == neigh_place[-1]:
                    neigh_place[index] = neigh_place[0]

        return conn_nb

    def kill_group(self,group):
        """
        given a group of coordonates, destroy the matching cells
        """
        for coord in group:
            if coord in self.board.keys():
                if self.board[coord][2]:
                    print("***ERROR in kill_group***")
                    print("there is a red piece in the group")
                    input()
                self.remove(coord)

    def connexity_update(self,coord):
        """
        after a move from coord, update connexity
        """
        neigh_list = self.get_neigh(coord)
        conn_nb = self.neigh_connex(coord)

        if conn_nb > 1:

            #print("from {}, checking conenxity for {}".format(coord, neigh_list))

            for neigh in neigh_list:


                connected = False

                #if len(self.get_red()) != 3:

                #    print("*** ERROR in connexity_update***")
                #    print("missing at least 1 dvonn piece")
                #    print(self)
                #    print(self.get_red())
                #    input()

                for dvonn in self.get_red():

                    connection, part = self.are_connected(neigh,dvonn)
                    if connection:
                        #print("{} is connected to {}".format(neigh,dvonn))
                        connected = True
                        break

                if not connected:

                    self.kill_group(part)

if __name__ == '__main__':

    test = DvonnGame()
    print(test)
    test.random_setup()
    print(test)
    print(test.opt[0])
    print(test.opt[1])
