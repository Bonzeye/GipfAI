# -*- coding: utf-8 -*-

from random import choice
from datetime import datetime

from tools.utils import get_delta_time
from engine.game import *

class Human:
    def __init__(self):

        self.name = input("Name ? ")

    def place(self,game):

        row = int(input("row ? "))
        col = int(input("col ? "))
        return "{} {} {}".format(game.player,row,col)

    def play(self,game):

        playable = game.possible_play()

        if playable == list():
            return 'pass'

        for coord in playable:
            print("moves for {}".format(coord))
            print(game.available_moves(coord))

        print("start")
        row1 = int(input("row ? "))
        col1 = int(input("col ? "))

        print("end")
        row2 = int(input("row ? "))
        col2 = int(input("col ? "))

        return "{} {} {} {} {}".format(game.player,row1,col1,row2,col2)

class RandomPlayer:
    def __init__(self):
        self.name = "RandomPlayer"
    def place(self,game):
        pass
    def play(self,game):

        playable = game.possible_play()

        if playable == list():
            return 'pass'

        start,end = choice(playable)


        return "{} {} {} {} {}".format(game.player,start[0],start[1],end[0],end[1])

if __name__ == '__main__':

    test = DvonnGame()
    delta = datetime.now()
    for i in range(1000):
        test = DvonnGame()
        test.run(RandomPlayer(),RandomPlayer(),verbose=False)
    print("Computed in {}".format(get_delta_time(delta)))
