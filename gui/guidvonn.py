# a simple gui for a human player

from tkinter import *
from math import sin, cos, pi

from engine.game import *

players = ["white","black"]

class GUI_player:
    def __init__(self):
        self.name="Player"
    def play(self,game):
        self.start = None
        self.end = None
        self.passing = False
        self.fen = Tk()

        GameView(self.fen,self,game).pack()

        self.fen.mainloop()

        if self.passing:
            return "pass"

        return "{} {} {} {} {}".format(game.player,self.start[0],self.start[1],self.end[0],self.end[1])


class GameView(Frame):
    def __init__(self,master,controler,game):
        Frame.__init__(self,master)
        # controlers
        self.ctrl = controler
        self.game = game
        # views
        self.bar = CtrlBar(self)
        self.can = HexCanvas(self)
        self.bar.pack()
        self.can.pack()


colors = {0:'white',1:'black',2:'red'}
text_colors = {0:'black',1:'white',2:'black'}

u = (1,0)
v = (cos(2*pi/3), sin(2*pi/3))
def base(coord,trans=(0,0),scale=10):
    """
    change base of a coordonate to the base (u,v) and with a trans translation
    """
    return ((coord[1]*u[0]+coord[0]*v[0])*3*scale+trans[0],(coord[0]*v[1])*3*scale+trans[1])

class HexCanvas(Canvas):
    def __init__(self,master,scale=30):

        n_rows = 5
        n_cols = 11

        Canvas.__init__(self,master,
                        height=((n_rows * sin(2*pi/3))*3*scale + 2*scale),
                        width=((n_cols * 3*scale) + (n_rows * cos(2*pi/3) * (-3*scale)) + 2*scale)
                        )

        self.master = master

        self.scale = scale
        self.translation = (n_rows * cos(2*pi/3) * (-3*scale) +scale , scale)

        font = DvonnGame()

        for coord in font.board.keys():
            mod_coord = base(coord,self.translation,scale)

            for neigh in font.get_neigh(coord):
                mod_neigh = base(neigh,self.translation,scale)
                self.create_line(mod_coord[0],mod_coord[1],mod_neigh[0],mod_neigh[1])

        for coord,cell in master.game.board.items():
            mod_coord = base(coord,self.translation,scale)

            if cell is not None:

                self.create_oval(mod_coord[0]-scale/2,mod_coord[1]-scale/2,mod_coord[0]+scale/2,mod_coord[1]+scale/2,fill=colors[cell[0]])

                if cell[2]:

                    self.create_oval(mod_coord[0]-scale/3,mod_coord[1]-scale/3,mod_coord[0]+scale/3,mod_coord[1]+scale/3,fill=colors[2])

                self.create_text(mod_coord[0],mod_coord[1],text=str(cell[1]),font='bold',fill=text_colors[cell[0]])

        for coord in master.game.opt[master.game.player]:

            mod_coord = base(coord,self.translation,scale)
            self.create_oval(mod_coord[0]-self.scale/1.5,mod_coord[1]-self.scale/1.5,mod_coord[0]+self.scale/1.5,mod_coord[1]+self.scale/1.5)



        self.bind("<1>",self.select)

    def select(self,event):

        if self.master.ctrl.start is None:

            for coord in self.master.game.opt[self.master.game.player]:

                mod_coord = base(coord,self.translation,self.scale)

                if (event.x-mod_coord[0])**2 + (event.y-mod_coord[1])**2 <= (self.scale/2)**2:

                    self.create_oval(mod_coord[0]-self.scale/1.5,mod_coord[1]-self.scale/1.5,mod_coord[0]+self.scale/1.5,mod_coord[1]+self.scale/1.5,width=3)
                    self.master.ctrl.start = coord

        elif self.master.ctrl.end is None:

            for coord in self.master.game.available_moves(self.master.ctrl.start):

                mod_coord = base(coord,self.translation,self.scale)

                if (event.x-mod_coord[0])**2 + (event.y-mod_coord[1])**2 <= (self.scale/2)**2:

                    self.create_oval(mod_coord[0]-self.scale/1.5,mod_coord[1]-self.scale/1.5,mod_coord[0]+self.scale/1.5,mod_coord[1]+self.scale/1.5,width=3)
                    self.master.ctrl.end = coord

class CtrlBar(Frame):
    def __init__(self,master):
        Frame.__init__(self,master)
        self.master = master
        self.current_player = Label(text=players[self.master.game.player]).pack()
        self.current_round = Label(text=str(self.master.game.round)).pack()
        self.validate = Button(text='Validate',command=self.validation).pack()
        self.cancel = Button(text='Cancel',command=self.cancel).pack()
        self.passe = Button(text='Pass',command=self.passing).pack()

    def cancel(self):
        self.master.can.destroy()
        self.master.can = HexCanvas(self.master)
        self.master.can.pack()
        self.master.ctrl.start = None
        self.master.ctrl.end = None

    def validation(self):
        self.master.master.destroy()

    def passing(self):
        if self.master.game.possible_play() == list():
            self.master.ctrl.passing = True
            self.master.master.destroy()
