
# This is the main file to access the project
# available argumnts are
#
from sys import argv

from engine.game import DvonnGame
from gui.guidvonn import GUI_player

from algo.mctreesearch import MCTS_player
from algo.mixplayer import MixPlayer
from algo.advancedMCTS import Bot_AdvancedMCTS

from parser.gamehandler import save_game,load_game

from copy import deepcopy

w_player = MCTS_player()
b_player = Bot_AdvancedMCTS(nb_sim=20,player=1,threshold=10,verbose=True)

game = DvonnGame()

if len(argv) is 1:
    score = game.run(w_player,b_player)
    print(score)
elif "-g" in argv[1]:
    print("generating configuration")
    game.random_setup()

    while game.round < 20:
        choice = b_player.play(game)
        print("round {} -> {}".format(game.round,choice))
        game.play(choice)

    save_game(game,argv[2])
elif "-l" in argv[1]:
    print("loading configuration")
    game = load_game(argv[2])
    game.generate_opt()
    print(game)
    if game.is_finished():
        print("Game is finished")
    score = game.run(w_player,b_player,verbose=True)
    print(score)

elif "-t" in argv[1]:

    game = DvonnGame()
    game.random_setup()
    result = [0,0]

    for i in range(10):

        w_player = MCTS_player()
        b_player = MixPlayer()

        print(i)
        copy_game = deepcopy(game)
        score = copy_game.run(w_player,b_player)
        if score[0] > score[1]:
            result[0] += 1
        elif score[0] < score[1]:
            result[1] += 1
        print(score)

        w_player = MCTS_player()
        b_player = MixPlayer()

        copy_game = deepcopy(game)
        score = copy_game.run(b_player,w_player)
        if score[0] > score[1]:
            result[1] += 1
        elif score[0] < score[1]:
            result[0] += 1
        print(score)

    print("MCTS wins = {}".format(result[0]))
    print("mix wins = {}".format(result[1]))
