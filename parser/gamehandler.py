from os.path import expanduser

from engine.game import DvonnGame

# saving and loading game instances

def save_game(game,filepath):
    """
    save instance of game to filepath
    """
    f = open(expanduser(filepath),'w')
    f.write(str(game.round)+"\n"+str(game.passing)+"\n")
    f.write(str(game))
    f.close()

def load_game(filepath):
    """
    return instance of game from filepath
    """
    f = open(expanduser(filepath),'r')
    text = f.readlines()
    f.close()

    game = DvonnGame()

    game.round = int(text[0])
    game.passing = int(text[1])
    game.player = game.round % 2
    game.state = "play"

    for line in text[2:]:
        row,col,sep,pl,num,red = line.split()
        row = int(row)
        col = int(col)
        pl = int(pl)
        num = int(num)
        if red == "False":
            red = False
        else:
            red = True
        game.board[(row,col)] = (pl,num,red)

    to_delete = list()
    for key,value in game.board.items():
        if value is None:
            to_delete.append(key)

    for key in to_delete:
        del game.board[key]

    game.generate_opt()

    return game
