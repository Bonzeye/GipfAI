# a specific manager to manage dvonn games with register text files

from copy import copy, deepcopy

from engine.game import *

class GameLog:

    """
    a simple structure to store games,
    with a setup for the original configuration
    and a choice_list for all the choices of the 2 players
    """

    def __init__(self,game=None):

        """
        initilization of the game log with the board of initial game
        """

        if game is not None:
            self.setup = deepcopy(game.board)
        else:
            self.setup = None

        self.choice_list = list()

    def __str__(self):

        text = list()
        text.append("setup")
        for coord, cell in self.setup.items():
            if cell is not None:
                text.append("{} {} : {} {} {}".format(coord[0],coord[1],cell[0],cell[1],cell[2]))
            else:
                text.append("{} {} : None".format(coord[0],coord[1]))
        text.append("play")
        for choice in self.choice_list:
            text.append(copy(choice))

        return "\n".join(text)

    def append(self,choice):

        """
        add the next choice to the log
        """

        self.choice_list.append(copy(choice))

def save_game(game_log,file_path):

    """
    register a specific game into a file
    """

    f = open(file_path,'w')
    f.write(str(game_log))
    f.close()

def load_game(file_path):

    """
    load a specific game previously stored into a file and then return it as a
    GameLog object
    """


    class DvonnGame:
        def __init__(self):
            self.board = dict()

    template_game = DvonnGame()

    f = open(file_path,'r')
    mode = None

    for line in f.readlines():

        if "setup" in line and mode is None:

            mode = "setup"

        elif "play" in line and mode is "setup":

            mode = "play"

            game_log = GameLog(template_game)

        elif mode is "setup":

            row, col, sep, player, size, dvonn = line.split()
            coord = (int(row),int(col))
            cell = (int(player),int(size),bool(dvonn))

            template_game.board[copy(coord)] = copy(cell)

        elif mode is "play":

            if "\n" not in line:
                game_log.append(line)
            else:
                game_log.append(line[:-1])

    return game_log
