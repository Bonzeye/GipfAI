from copy import deepcopy

class TreeNode:

    def __init__(self,father=None,player=0):

        if father is None:
            self.father = self
            self.depth = int()
        else:
            self.father = father
            self.depth = self.father.depth + 1

        self.sons = dict()

        self.alpha = -50
        self.beta = 50
        self.value = None
        self.complete = False

        if father is None:
            self.comp = player
        else:
            self.comp = (self.father.comp + 1) % 2

    def __str__(self):

        text = list()
        if self.value is None:
            value = "None"
        else:
            value = str(self.value)
        text.append(" "*self.depth + self.label() + ":" + value)
        for son in self.sons.values():
            text.append(str(son))
        return "\n".join(text)

    def add_son(self,string_node,value=None):

        if not string_node in self.sons.keys():

            self.sons[string_node] = TreeNode(father=self)
            self.sons[string_node].value = value

    def del_son(self,string_node):

        if string_node in self.sons.keys():

            del self.sons[string_node]

    def is_leaf(self):

        if self.sons == dict():
            return True
        return False

    def label(self):

        for label,node in self.father.sons.items():
            if node is self:
                return label
        return "None"

    def alpha_beta(self,alpha=-50,beta=50,verbose=False):

        """
        alpha-beta pruning minimax algorithm
        """

        if verbose:
            print("Computing value of node {}".format(self.label()))
            print("\talpha={}".format(alpha))
            print("\tbeta={}".format(beta))

        self.alpha = alpha
        self.beta = beta

        if self.is_leaf():

            if verbose:
                print("Node is a leaf")
                print("Value of node = {}".format(self.value))

            return self.value

        if self.complete and self.value is not None:

            if verbose:
                print("Node is already minimaxed")
                print("Value of node = {}".format(self.value))

            return self.value

        elif self.comp is 1:

            self.value = 50

            for son in self.sons.values():

                self.value = min(self.value,son.alpha_beta(self.alpha,self.beta,verbose=verbose))

                if self.value <= self.alpha:

                    if verbose:
                        print("Alpha pruning of node {} with value {}".format(self.label(),self.value))
                    return self.value

                self.beta = min(self.beta,self.value)

        else:

            self.value = -50

            for son in self.sons.values():

                self.value = max(self.value,son.alpha_beta(self.alpha,self.beta,verbose=verbose))

                if self.value >= self.beta:

                    if verbose:
                        print("Beta pruning of node {} with value {}".format(self.label(),self.value))

                    return self.value

                self.alpha = max(self.alpha,self.value)

        if verbose:
            print("Computing of node {} complete without pruning. Value = {}".format(self.label(),self.value))

        return self.value

    def build_from_game(self,game,verbose=False,hashing=None,tt=None):

        """
        as for gametree object, build full tree from a game with methods :
        play
        possible_play
        is_finished
        score
        can use a hash function and a matching transposition table
        if hashing is a hashing class with a hash method
        and tt is a corresponding transposition table
        """

        if (hashing is not None and tt is None) or ((hashing is None and tt is not None)):
            raise ValueError("Need both hashing object and transposition table")

        if game.is_finished():

            self.value = game.score()[0] - game.score()[1]

            if verbose:
                print("Leaf reached : {}".format(self.value))

        else:

            possibilities = game.possible_play()

            if possibilities == list():

                copy_game = deepcopy(game)
                choice = "pass"

                if verbose:
                    print("from {}, exploring {}".format(self.label(),choice))

                self.add_son(choice)
                copy_game.play(choice)
                self.sons[choice].build_from_game(deepcopy(copy_game),verbose=verbose)

            else:

                for start,end in possibilities:
                    copy_game = deepcopy(game)
                    choice = "{} {} {} {} {}".format(game.player,start[0],start[1],end[0],end[1])

                    if verbose:
                        print("from {}, exploring {}".format(self.label(),choice))

                    copy_game.play(choice)

                    if hashing is not None:
                        hash_value = hashing.hash(copy_game)
                        if hash_value in tt.keys():
                            self.sons[choice] = tt[hash_value]
                        else:
                            self.add_son(choice)
                            tt[hash_value] = self.sons[choice]
                    else:
                        self.add_son(choice)

                    self.sons[choice].build_from_game(deepcopy(copy_game),verbose=verbose)

        self.complete = True


class GameTree:

    def __init__(self,player=0):

        self.root = TreeNode(player=player)
        self.current = self.root
        self.size = int(1)

    def __str__(self):

        return str(self.root)

    def add_son(self,string_node,value=None):

        self.current.add_son(string_node,value)
        self.size += 1

    def del_son(self,string_node):

        self.current.del_son(string_node)

    def get_son(self,string_node):

        return self.sons[string_node]

    def get_sons(self):

        """
        return all sons of current node
        """

        return list(self.current.sons.keys())

    def go_father(self):

        self.current = self.current.father

    def go_son(self,string_node):

        if string_node in self.current.sons.keys():

            self.current = self.current.sons[string_node]

    def go_brother(self,string_node):

        self.go_father()
        self.go_son(string_node)

    def go_root(self):

        self.current = self.root

    def is_leaf(self):

        return self.current.is_leaf()

    # more advanced algorithm

    def minimax(self,verbose=False):

        return self.current.alpha_beta(verbose=verbose)

    def best_son(self,verbose=False):

        if verbose:
            print("### BEST SON OF {}###".format(self.current.label()))
            input()

        res = dict()
        for string_node,cell in self.current.sons.items():
            res[string_node] = cell.alpha_beta(verbose=verbose)

        if self.current.comp is 0:
            best = max(res.values())
        else:
            best = min(res.values())

        for key,value in res.items():
            if value == best:
                return key

    def full_path(self):
        """
        return the full path from root to node
        """
        res = list()
        while not self.current is self.root():
            res.append(self.current.label())
            self.go_father()
        res.reverse()
        return res

    def build_from_game(self,game,verbose=False,hashing=None,tt=None):

        # build a tree from an instance of a game with specific methods
        # game.play
        # game.is_finished
        # game.possible_play

        self.current.build_from_game(game,verbose=verbose,hashing=hashing,tt=tt)



class TreeTest:
    """
    a specific class to test gametrees
    """
    def __init__(self):
        self.tree = GameTree()
        self.test_list = ["build basic tree",
                          "sample minimax computing"]

    def base_tree(self):

        print("Building sample tree.")

        self.tree.add_son("a1")
        self.tree.add_son("a2")
        self.tree.add_son("a3")

        self.tree.go_son("a1")
        self.tree.add_son("b1")
        self.tree.add_son("b2")

        self.tree.go_son("b1")
        self.tree.add_son("c1")
        self.tree.add_son("c2")

        self.tree.go_son("c1")
        self.tree.add_son("d1",value=5)
        self.tree.add_son("d2",value=6)

        self.tree.go_brother("c2")
        self.tree.add_son("d3",value=7)
        self.tree.add_son("d4",value=4)
        self.tree.add_son("d5",value=5)

        self.tree.go_father()
        self.tree.go_brother("b2")
        self.tree.add_son("c3")
        self.tree.go_son("c3")
        self.tree.add_son("d6",value=3)

        self.tree.go_root()
        self.tree.go_son("a2")
        self.tree.add_son("b3")
        self.tree.add_son("b4")

        self.tree.go_son("b3")
        self.tree.add_son("c4")
        self.tree.add_son("c5")

        self.tree.go_son("c4")
        self.tree.add_son("d7",value=6)
        self.tree.go_brother("c5")
        self.tree.add_son("d8",value=6)
        self.tree.add_son("d9",value=9)
        self.tree.go_father()
        self.tree.go_brother("b4")
        self.tree.add_son("c6")
        self.tree.go_son("c6")
        self.tree.add_son("d10",value=7)

        self.tree.go_root()
        self.tree.go_son("a3")
        self.tree.add_son("b5")
        self.tree.add_son("b6")

        self.tree.go_son("b5")
        self.tree.add_son("c7")
        self.tree.go_son("c7")
        self.tree.add_son("d11",value=5)

        self.tree.go_father()
        self.tree.go_brother("b6")
        self.tree.add_son("c8")
        self.tree.add_son("c9")
        self.tree.go_son("c8")
        self.tree.add_son("d12",value=9)
        self.tree.add_son("d13",value=8)
        self.tree.go_brother("c9")
        self.tree.add_son("d14",value=6)

        self.tree.go_root()

        print("Tree building complete.")

    def minimax_sample(self):

        self.tree.go_root()

        print("Starting minimax computing.")

        result = self.tree.minimax(verbose=True)

        print("Minimax computing complete with value {}".format(result))

if __name__ == '__main__':

    test = TreeTest()

    test.base_tree()

    test.minimax_sample()
