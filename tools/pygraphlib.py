import os,sys
from os.path import exists,expanduser
from collections import deque

""" A proper graph implementation for computational purpose """

class Edge:
    """
    like the name can suppose
    we suppose we don't need to add specific getters/setters
    """

    def __init__(self, start, end, weight=1,label=""):
        self.start = start
        self.end = end
        self.weight = weight
        self.label = label

    def getVertices(self):
        """ to get the 2 vertices as a tuple """
        return self.start, self.end

class Graph:
    """
    a graph is defined by a dequeue of its vertices and a liost of edges
    the mostly used order is the deque index as a reference to the vertex
    edges are stored in a list for sorting purpose
    """


    def __init__(self):
        self.n_vertices = int()
        self.n_edges = int()
        self.vertices = deque()
        self.edges = list()

    def view(self):
        print("Graph with {} vertices and {} edges.".format(self.n_vertices,
            self.n_edges))
        print("Vertices are :")
        for i in range(self.n_vertices):
            print("{}:{}".format(i,self.vertices[i]))
        print("Edges are :")
        for edge in self.edges:
            print("{} from {} to {}, weight = {}".format(edge.label,
                edge.start,edge.end,edge.weight))

    def addVertex(self, vertex):
        self.vertices.append(vertex)
        self.n_vertices += 1

    def addEdge(self, start, end, weight=1, label=""):
        """ start and edge should be references to node, not specific node """
        self.edges.append(Edge(start,end,weight,label))
        self.n_edges += 1

    def indexVrtx(self, vertex):
        """ give the index of the vertex in the deque """
        return self.vertices.index(vertex)

    def sortbyWeight(self):
        """ sort the edges by weight (increasing)"""
        self.edges.sort(key=lambda ed:ed.weight)

    def to_tikz(self,file_path):
        """ return a formatted string or file to output graph in tikz """
        text = open(expanduser(file_path),"w")
        text.write("\\documentclass{article}\n")
        text.write("\\usepackage{tikz}\n")
        text.write("\\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}\n")
        text.write("\\begin{document}\n")
        text.write("\\centering \large{\tt plop.tex}\n")

        text.write("\\begin{tikzpicture}[scale=0.3]\n")
        text.write("\\tikzset{sommet/.style={draw,circle,thick=2pt,minimum height=.55cm}}\n")
        text.write("\\tikzset{arete/.style={thick=2pt}}\n")
        text.write("\\tikzset{areteillustree/.style={line width=2.5pt,color)lightgray}}\n")

        for i in range(self.n_vertices):

            x,y = self.vertices[i].split()
            x,y = float(x),float(y)
            text.write("\\node[sommet] (V%d) at (%f,%f) {}; \draw(V%d) node{\\tt %d};\n" % (i,x,y,i,i))

        for edge in self.edges:
            text.write("\\draw[arete] (V{0}) to (V{1});\n".format(edge.start,edge.end))

        text.write("\\end{tikzpicture}")

        text.write("\\end{document}")

        text.close()

    def parse(self, file_path):
        """ will build the graph from a file """
        if not exists(expanduser(file_path)):
            raise IOError("Connot find {} on disk".format(file_path))
        with open(expanduser(file_path), "r") as pipe:
            for i,line in enumerate(pipe):
                parts = line.split()
                if i == 0:
                    n_vertices = int(parts[0])
                    n_edges = int(parts[1])
                elif 0 < i <= n_vertices:
                    self.addVertex(line)
                else:
                    self.addEdge(int(parts[0])-1,int(parts[1])-1,
                        weight = float(parts[2]))
            if not (self.n_edges == n_edges and self.n_vertices == n_vertices):
                raise ExceptionError("File not matching number of edges/vertices")
