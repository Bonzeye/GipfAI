# -*- coding: utf-8 -*-
# ------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
# ------------------------------------------------------------------

# ------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------

from datetime import datetime

# ------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------

class Color(object):
    Black       = 0
    Red         = 1
    Green       = 2
    Yellow      = 3
    Blue        = 4
    Magenta     = 5
    Cyan        = 6
    White       = 7

# ------------------------------------------------------------------
#   Functions
# ------------------------------------------------------------------

def color(text, foreground, bold=True):
    """
    Show a text with some colors with ANSI escape codes

    :param str text: Text value
    :param str foreground: Text color
    :param bool bold: Use bold style ( Default True )
    """

    if bold:
        foreground = "%s;1" % str(foreground)

    print("\x1b[3%sm%s\x1b[0m" % (str(foreground), text))


def get_delta_time(date):
    """
    Return a string with delta time since a specific date

    :param datetime date: Date to compare

    :return: String
    :rtype: str
    """

    delta = datetime.now() - date

    return "%ds %dms" % (delta.seconds, delta.microseconds)
