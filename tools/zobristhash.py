from random import getrandbits

from engine.game import DvonnGame

# zobrist hashing implementation for dvonn game

class Hash:
    def __init__(self):
        """
        initialize with a random table (of 512 bytestring)
        table is 49 row (for every case of the board)
        and 62 rows :
        0 is empty
        1 is for Dvonn piece alone
        2-16 is for a white player pile of size (n-1) without Dvonn Piece
        17-31 is for a white player pile of size (n-16) with DvonnPiece
        32-46 is for black player pile of size (n-31) without DvonnPiece
        47-61 is for black player pile of size (n-46) with DvonnPiece
        and there is a bytestring for current player also
        """
        self.hashtab = [[getrandbits(512) for i in range(62)] for i in range(49)]
        self.players = [getrandbits(512),getrandbits(512)]
        self.row_map = dict()
        self.col_map = dict()


        # building mapping row <> coordonates of a cell
        tmp_game = DvonnGame()
        l = list(tmp_game.board.keys())
        l.sort()

        for i in range(49):
            self.row_map[l[i]] = i

        # building mapping col <> pile of a cell
        self.col_map[(2,1,True)] = 1
        for i in range(15):
            self.col_map[(0,i,False)] = i+1
            self.col_map[(0,i,True)] = i+16
            self.col_map[(1,i,False)] = i+31
            self.col_map[(1,i,True)] = i+46

    def hash(self,game):
        """
        compute the hash value of a DvonnGame
        """
        result = int()
        for coord,cell in game.board.items():
            result = result ^ self.hashtab[self.row_map[coord]][self.col_map[cell]]
        result = result ^ self.players[game.player]
        return result
